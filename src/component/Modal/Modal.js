import React, {Component} from 'react';
import './Modal.scss';
import cross from '../../images/cross.png';

class Modal extends Component {

    render() {
    const {onClick} = this.props;
    const wrapperClose = (e) => {
            if(e.target.getAttribute('class') === "ModalWrapper") {
                onClick();
            }
            console.log(e.target.getAttribute('class'))
        };
        return (
             <div className="ModalWrapper" onClick={wrapperClose}>
               <div className={`Modal ${this.props.modalOne ? 'ModalOne' : 'ModalTwo'}`}>
                    <div className={`Modal__header ${this.props.modalOne ? 'ModalOneHeader' : 'ModalTwoHeader'}`}>
                        <p className="Modal__header-text">{this.props.header}</p>
                        <span><img src={cross} alt=""/></span>
                    </div>
                    <div>
                        <p className="Modal__main">{this.props.text}</p>
                        <div className="Modal__buttons">{this.props.actions}</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;