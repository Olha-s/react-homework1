import React, {Component} from 'react';
//import logo from './logo.svg';
import './App.css';
//import Counter from './component/Counter/Counter';
import Button from './component/Button/Button';
import Modal from './component/Modal/Modal';
//import Modal from './component/Modal/Modal.scss';
//import ButtonModal from "./component/Button/ButtonModal";


class App extends Component {
    state = {
       modalShow: false,
       modalShowOne: false,
       modalShowTwo: false
    };
    modalOpen = () =>{
        this.setState({modalShow: true})
    };
    modalOpenOne = () =>{
        this.setState({modalShowOne: true})
    };
    modalOpenTwo = () =>{
        this.setState({modalShowTwo: true})
    };
    modalClose = () =>{
        this.setState({modalShowOne: false});
        this.setState({modalShowTwo: false});
       };

render() {

    return (
        <div className="App">
            <div className="Main-buttons">
            <Button text={"Open first modal"} onClick={this.modalOpenOne} backgroundColor={"#B3382C"}/>
            <Button text={"Open second modal"} onClick={this.modalOpenTwo} backgroundColor={"#000099"}/>
            </div>
            {this.state.modalShowOne && <Modal
                modalOne = {true}
                onClick={this.modalClose}
                header={"Do you want to delete this file?"}
                closeButton ={true}
                text={"Once you delete this file, it won’t be possible to undo this action.  Are you sure you want to delete it?"}
                actions={
                    <>
                      <button className={"Modal__btn-1"}>Ok</button>
                      <button className={"Modal__btn-1"}>Cancel</button>
                    </>
                }
            />}
            {this.state.modalShowTwo && <Modal
                modalTwo = {true}
                onClick={this.modalClose}
                header={"Are you sure you want to do that?"}
                closeButton ={true}
                text={"Clicking Yes will make Comic Sans your new system  default font. Seriously, have you thought your through?"}
                actions={
                     <>
                        <button className={"Modal__btn-2"}>Yes</button>
                        <button className={"Modal__btn-2"}>No</button>
                     </>
                }
            />}
            </div>
    );
}
}

export default App;
